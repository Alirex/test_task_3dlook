# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'DO_YOUR_UNIQUE_KEY_with_50+_chars'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '',  # Database name
        'USER': '',  # Database user_name
        'PASSWORD': '',  # Database user_password
        'HOST': '127.0.0.1',  # Address, where Database located
        'PORT': '5432',  # Port on address
        'TEST': {
            'NAME': '',  # Name for test database. Also you need to grant permission for USER to "create" database(DB) or grant privileges on current DB. It depends from DBMS.
        },
    }
}

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Enter your domain name and/or ip
ALLOWED_HOSTS = ['127.0.0.1']
