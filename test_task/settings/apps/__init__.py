# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.sites',
]

CUSTOM_APPS = [
    'product.apps.ProductConfig',
    'likes.apps.LikesConfig',
]
INSTALLED_APPS += CUSTOM_APPS

EXTRA_APPS = [
    'django_comments',
    'vote',
    'rest_framework',
]
INSTALLED_APPS += EXTRA_APPS
