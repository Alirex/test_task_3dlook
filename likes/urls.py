from django.conf.urls import url
from . import views

app_name = 'likes'
urlpatterns = [
    url(r'^like/(?P<content_type>[-\w]+)/(?P<obj_id>[0-9]+)/$', views.LikeToggle.as_view(), name='like_toggle'),
]
