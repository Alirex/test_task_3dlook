from django.shortcuts import render, redirect, reverse, get_object_or_404
from . import services
from django.views.generic import RedirectView
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages


class LikeToggle(LoginRequiredMixin,
                 RedirectView,):

    def get_redirect_url(self, *args, **kwargs):

        # ---[BEGIN]-[Get object for like]---
        obj_id = self.kwargs.get("obj_id")
        content_type = self.kwargs.get("content_type")
        obj_model = ContentType.objects.get(model=content_type).model_class()
        obj = get_object_or_404(obj_model, pk=obj_id)
        # ---[END]-[Get object for like]---

        # ---[BEGIN]-[Action + Message]---
        if services.is_fan(obj, self.request.user):
            services.remove_like(obj, self.request.user)
            message = f'{obj} unliked :('
        else:
            services.add_like(obj, self.request.user)
            message = f'{obj} liked :)'

        messages.success(request=self.request, message=message)
        # ---[END]-[Action + Message]---

        url = obj.get_absolute_url()

        return url
