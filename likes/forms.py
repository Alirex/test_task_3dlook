from django import forms


class LikeChangeForm(forms.Form):
    content_type = forms.CharField(widget=forms.HiddenInput)
    obj_id = forms.IntegerField(widget=forms.HiddenInput)

    user_id = forms.IntegerField(widget=forms.HiddenInput)
