from django.contrib.contenttypes.fields import GenericRelation
from .models import Like
from django.db import models
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType


class LikeMixinModel(models.Model):
    likes = GenericRelation(Like)
    total_likes = models.IntegerField(default=0)

    @property
    def get_content_type(self):
        return ContentType.objects.get_for_model(self)

    def count_total_likes(self):
        return self.likes.count()

    def get_like_url(self):
        return reverse('likes:like_toggle', kwargs={'content_type': self.get_content_type, 'obj_id': self.pk})

    @classmethod
    def fix_product_likes_count(cls):
        """
        Use if field "total_likes" created after the "likes" were already user
        :return:
        """
        for product in cls.objects.all():
            if product.total_likes != product.count_total_likes():
                product.total_likes = product.count_total_likes()
                product.save()

    class Meta:
        abstract = True