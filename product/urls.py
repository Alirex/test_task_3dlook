from django.conf.urls import url, include
from . import views

app_name = 'product'
urlpatterns = [
    url(r'^$', views.ProductListView.as_view(), name='catalog'),
    url(r'^(?P<slug>[-\w]+)/$', views.ProductDetailView.as_view(), name='detail'),
]