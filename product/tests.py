from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from product.models import Product
from django_comments.models import Comment
from unittest.mock import patch
from django.utils import timezone
from django.contrib.sites.models import Site


class ProductDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_product = Product.objects.create(
            slug='test_product',
            name='Test product',
            description='This is my test product',
            price=9.99
        )

        comment_data = {
            'content_type': cls.test_product.get_content_type,
            'object_pk': cls.test_product.pk,
            'user_name': 'Test user',
            'user_email': 'test@gmail.com',
            'comment': 'Lorem Ipsum',
            'site': Site.objects.get_current(),
        }

        testtime = timezone.now() - timezone.timedelta(hours=25)

        with patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = testtime
            outdate_comment = Comment.objects.create(**comment_data)

        actual_comment = Comment.objects.create(**comment_data)

    def setUp(self):
        # initialize "dummy client" for "view" test
        self.client = Client()

    def test_is_view_ok(self):
        # Issue a GET request
        response = self.client.get(reverse('product:detail', kwargs={'slug': 'test_product'}))

        self.assertEqual(response.status_code, 200, 'Response is not "200 OK"')

    def test_comment_filter(self):
        response = self.client.get(reverse('product:detail', kwargs={'slug': 'test_product'}))

        self.assertEqual((len(response.context['comments'])), 1, 'Bad comment filtering')




