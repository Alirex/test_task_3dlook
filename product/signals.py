from django.dispatch import receiver
from django_comments.signals import comment_was_posted
from django.contrib import messages


@receiver(comment_was_posted)
def send_message(sender, **kwargs):
    messages.success(request=kwargs['request'], message='Comment posted')
