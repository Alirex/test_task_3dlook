from .models import Product
from django_comments.models import Comment
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from .utils import SortDictManager, SortOptionInput
from django.views.generic import ListView, DetailView, TemplateView


class ProductListView(ListView):
    model = Product
    template_name = 'product/catalog/__init__.html'
    paginate_by = 5

    SORT_NAME = 'sort'
    valid_sortings = SortDictManager(
        SortOptionInput(_('Primary Key'), 'pk', 'pk'),
        SortOptionInput(_('Likes'), 'likes', 'total_likes'),
    )

    def get_queryset(self):
        sort_option = self.request.GET.get(self.SORT_NAME)
        sort_key_for_django_orm = self.valid_sortings.get(sort_option)
        new_context = self.model.objects.all().order_by(sort_key_for_django_orm)
        return new_context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        sort_option = self.request.GET.get(self.SORT_NAME)
        context[self.SORT_NAME] = self.valid_sortings.get(sort_option)
        context['sort_settings'] = self.valid_sortings.get_data_for_form()

        return context


class ProductDetailView(DetailView):
    model = Product
    template_name = 'product/detail/__init__.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comments'] = Comment.objects\
            .for_model(Product)\
            .filter(
                    object_pk=self.object.pk,
                    submit_date__gte=(timezone.now()-timezone.timedelta(hours=24))
                    )\
            .order_by('-submit_date')
        return context


class HomePageView(TemplateView):
    template_name = 'product/main_page.html'

