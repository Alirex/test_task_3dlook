import collections


SortOptionInput = collections.namedtuple('SortOptionInput', ['verbose_name', 'input_key', 'output_key'])
SortOptionStorage = collections.namedtuple('SortOptionStorage', ['output_key', 'verbose_name'])
SortInputQuery = collections.namedtuple('SortInputQuery', ['input_key', 'direction'])


class SortDictManager:
    __ASC = 'asc'
    __ASC_ORM = ''
    __ASC_VERBOSE = 'ascending'
    __DESC = 'desc'
    __DESC_ORM = '-'
    DESC_VERBOSE = 'descending'

    __DIRECTIONS = {
        __ASC: SortOptionStorage(__ASC_ORM, __ASC_VERBOSE),
        __DESC: SortOptionStorage(__DESC_ORM, DESC_VERBOSE),
    }
    __DIRECTIONS = collections.OrderedDict(sorted(__DIRECTIONS.items(), key=lambda t: t[0]))

    __DEFAULT_DIRECTION = __ASC

    __DELIMETER = '__'

    def __init__(self, *args: SortOptionInput):

        self.core_dict = {}  # contains "SortOptionStorage" items
        self.default_key = None
        self.default_direction = self.__DEFAULT_DIRECTION
        for item in args:
            self.update(item)

    def _parse_string_to_input_query(self, input_string):
        input_data = input_string.split(self.__DELIMETER)

        return self._format_input_data_to_input_query(input_data)

    def _format_input_data_to_input_query(self, input_data):
        output_data = []
        try:
            if input_data[0] in self.core_dict:
                output_data.append(input_data[0])
            else:
                raise ValueError
        except (IndexError, ValueError):
            output_data.append(self.default_key)

        try:
            if input_data[1] in self.__DIRECTIONS:
                output_data.append(input_data[1])
            else:
                raise ValueError
        except (IndexError, ValueError):
            output_data.append(self.default_direction)

        return SortInputQuery(*output_data)

    def _normalize_input_data_to_input_query(self, *args):
        if (len(args) == 1) and (isinstance(*args, str)):
            input_query = self._parse_string_to_input_query(*args)
        else:
            input_query = self._format_input_data_to_input_query(args)
        return input_query

    def get_data_for_form(self):
        output_data = collections.OrderedDict()
        for input_key, value in self.core_dict.items():
            for direction_key, direction_value in self.__DIRECTIONS.items():

                joined_input_key = self.__DELIMETER.join([input_key, direction_key])
                joined_verbose = ' '.join([str(value.verbose_name), str(direction_value.verbose_name)])
                joined_output_key = ''.join([value.output_key, direction_value.output_key])

                output_data[joined_input_key] = SortOptionStorage(joined_output_key, joined_verbose)

        return output_data

    def get(self, *args):
        """
        Get key for DjangoORM sorting
        """
        input_query = self._normalize_input_data_to_input_query(*args)

        output_key = self.core_dict[input_query.input_key].output_key
        output_direction = self.__DIRECTIONS[input_query.direction].output_key
        output_query = f'{output_direction}{output_key}'

        return output_query

    def update(self, data: SortOptionInput):
        """
        Create or update core value for sorting
        """
        self.core_dict[data.input_key] = SortOptionStorage(data.output_key, data.verbose_name)
        if self.default_key is None:
            self.default_key = data.input_key
        return None
