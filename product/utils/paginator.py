from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def paginator(request, item_list, number_of_elements_on_page):

    paginator_obj = Paginator(item_list, number_of_elements_on_page)
    page = request.GET.get('page')
    try:
        items = paginator_obj.page(page)
    except PageNotAnInteger:
        items = paginator_obj.page(1)
    except EmptyPage:
        items = paginator_obj.page(paginator_obj.num_pages)

    return items