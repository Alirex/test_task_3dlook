from django.db import models
from likes.mixins import LikeMixinModel
from django.urls import reverse


class Product(LikeMixinModel):
    slug = models.SlugField(max_length=200, unique=True)

    name = models.CharField(max_length=200)
    description = models.TextField()

    price = models.DecimalField(max_digits=10, decimal_places=2)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse('product:detail', kwargs={'slug': str(self.slug)})
