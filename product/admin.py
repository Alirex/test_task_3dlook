from django.contrib import admin
from .models import Product
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _


class ViewOnSiteMixin(object):
    """
    Use if need to display link "View on site" in "list" of objects in "Admin Panel"

    Setup:
    Add this mixin to "model Admin" class
    Add function name (as string) in "list_display" of "model Admin"
    """
    def view_on_site__for_list(self, obj):
        return mark_safe(
            '<a href="{0}">{1}</a>'.format(
                obj.get_absolute_url(),
                _("View on site")
            )
        )

    view_on_site__for_list.allow_tags = True
    view_on_site__for_list.short_description = _("View on site")


@admin.register(Product)
class ProductAdmin(ViewOnSiteMixin, admin.ModelAdmin):
    readonly_fields = ('pk', 'created_at', 'modified_at')
    fieldsets = [
        (
            'Identification', {
                'fields': ('pk', 'slug'),
                'classes': ('collapse',),
            }
        ),
        (
            'Main', {
                'fields': ('name', 'description', 'price'),
            }
        ),
        (
            'Other', {
                'fields': ('created_at', 'modified_at'),
                'classes': ('collapse',),
            }
        ),
    ]

    prepopulated_fields = {
        'slug': ('name',)
    }

    list_display = ['name', 'view_on_site__for_list', ]


