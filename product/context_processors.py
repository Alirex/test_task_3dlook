from django.urls import reverse

import collections

MenuItem = collections.namedtuple('MenuItem', ['name', 'url'])

menu_main_data = [
    MenuItem(
        'Home',
        reverse('main_page')
    ),
    MenuItem(
        'Products',
        reverse('product:catalog')
    ),
]


def main_menu(request):

    return {
        'main_menu': menu_main_data,
    }
